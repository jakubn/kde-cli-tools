msgid ""
msgstr ""
"Project-Id-Version: kstart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-17 00:51+0000\n"
"PO-Revision-Date: 2020-06-03 00:52+0100\n"
"Last-Translator: Pedro Morais <morais@kde.org>\n"
"Language-Team: pt <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Spell-Extra: KStart\n"
"X-POFile-SpellExtra: TopMenu grep Dialog WMCLASS Tool KStart Desktop Dock\n"
"X-POFile-SpellExtra: Kicker xprop Override Faure Ettrich Toolbar Matthias\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: stdout service desktop application\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: kstart.cpp:255
#, kde-format
msgid "KStart"
msgstr "KStart"

#: kstart.cpp:257
#, kde-format
msgid ""
"Utility to launch applications with special window properties \n"
"such as iconified, maximized, a certain virtual desktop, a special "
"decoration\n"
"and so on."
msgstr ""
"Um utilitário para lançar as aplicações com propriedades especiais,\n"
"como, por exemplo, lançá-las minimizadas, maximizadas, num determinado\n"
"ecrã virtual, com uma decoração especial, e assim por diante."

#: kstart.cpp:262
#, kde-format
msgid "(C) 1997-2000 Matthias Ettrich (ettrich@kde.org)"
msgstr "(C) 1997-2000 Matthias Ettrich (ettrich@kde.org)"

#: kstart.cpp:264
#, kde-format
msgid "Matthias Ettrich"
msgstr "Matthias Ettrich"

#: kstart.cpp:265
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kstart.cpp:266
#, kde-format
msgid "Richard J. Moore"
msgstr "Richard J. Moore"

#: kstart.cpp:271
#, kde-format
msgid "Command to execute"
msgstr "Comando a executar"

#: kstart.cpp:275
#, kde-format
msgid ""
"Alternative to <command>: desktop file path to start. D-Bus service will be "
"printed to stdout. Deprecated: use --application"
msgstr ""
"Alternativa ao <comando>: ficheiro 'desktop' a iniciar. O serviço de D-Bus "
"será impresso no 'stdout'. Obsoleto: use o --application"

#: kstart.cpp:278
#, kde-format
msgid "Alternative to <command>: desktop file to start."
msgstr "Alternativa ao <comando>: ficheiro 'desktop' a iniciar."

#: kstart.cpp:281
#, kde-format
msgid "Optional URL to pass <desktopfile>, when using --service"
msgstr "URL opcional a passar ao <ficheiro desktop>, ao usar o '--service'"

#: kstart.cpp:284
#, kde-format
msgid "A regular expression matching the window title"
msgstr "Uma expressão regular condizente com o título da janela"

#: kstart.cpp:286
#, kde-format
msgid ""
"A string matching the window class (WM_CLASS property)\n"
"The window class can be found out by running\n"
"'xprop | grep WM_CLASS' and clicking on a window\n"
"(use either both parts separated by a space or only the right part).\n"
"NOTE: If you specify neither window title nor window class,\n"
"then the very first window to appear will be taken;\n"
"omitting both options is NOT recommended."
msgstr ""
"Um texto a indicar o classe da janela (propriedade WM_CLASS).\n"
"A classes da janela pode ser obtida executando\n"
"'xprop | grep WM_CLASS' e carregando na janela.\n"
"(utilize ambas as partes separadas por espaço ou só a da direita).\n"
"NOTA: Se não indicar nem o título da janela nem a classe,\n"
"a primeira janela a aparecer será tomada em conta;\n"
"NÃO é recomendado que omita as duas opções."

#: kstart.cpp:295
#, kde-format
msgid "Desktop on which to make the window appear"
msgstr "O ecrã onde a janela aparece"

#: kstart.cpp:297
#, kde-format
msgid ""
"Make the window appear on the desktop that was active\n"
"when starting the application"
msgstr ""
"Fazer a janela aparecer no ecrã que estava activo\n"
"quando a aplicação foi iniciada"

#: kstart.cpp:298
#, kde-format
msgid "Make the window appear on all desktops"
msgstr "Fazer a janela aparecer em todos os ecrãs"

#: kstart.cpp:299
#, kde-format
msgid "Iconify the window"
msgstr "Minimizar a janela"

#: kstart.cpp:300
#, kde-format
msgid "Maximize the window"
msgstr "Maximizar a janela"

#: kstart.cpp:301
#, kde-format
msgid "Maximize the window vertically"
msgstr "Maximizar a janela verticalmente"

#: kstart.cpp:302
#, kde-format
msgid "Maximize the window horizontally"
msgstr "Maximizar a janela horizontalmente"

#: kstart.cpp:303
#, kde-format
msgid "Show window fullscreen"
msgstr "Mostrar a janela no ecrã completo"

#: kstart.cpp:305
#, kde-format
msgid ""
"The window type: Normal, Desktop, Dock, Toolbar, \n"
"Menu, Dialog, TopMenu or Override"
msgstr ""
"O tipo de janela: Normal, Desktop, Dock, Toolbar, \n"
"Menu, Dialog, TopMenu ou Override"

#: kstart.cpp:308
#, kde-format
msgid ""
"Jump to the window even if it is started on a \n"
"different virtual desktop"
msgstr ""
"Saltar para a janela, mesmo que esta seja iniciada\n"
"num ecrã virtual diferente"

#: kstart.cpp:311
#, kde-format
msgid "Try to keep the window above other windows"
msgstr "Tentar que janela fique por cima de todas as outras"

#: kstart.cpp:313
#, kde-format
msgid "Try to keep the window below other windows"
msgstr "Tentar que a janela fique por baixo de todas as outras"

#: kstart.cpp:314
#, kde-format
msgid "The window does not get an entry in the taskbar"
msgstr "A janela não aparece na barra de tarefas"

#: kstart.cpp:315
#, kde-format
msgid "The window does not get an entry on the pager"
msgstr "A janela não aparece na miniatura dos ecrãs"

#: kstart.cpp:329
#, kde-format
msgid "No command specified"
msgstr "Não foi especificado nenhum comando"
