# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kde-cli-tools package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kde-cli-tools\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-15 00:57+0000\n"
"PO-Revision-Date: 2023-01-07 08:20+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Temuri Doghonadze"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: filegroupdetails.cpp:22
#, kde-format
msgid "Left Click Action (only for Konqueror file manager)"
msgstr "მარცხენა წკაპის ქმედება (მხოლოდ Konqueror-ის ფაილის მმარველისთვის)"

#: filegroupdetails.cpp:26 filetypedetails.cpp:133
#, kde-format
msgid "Show file in embedded viewer"
msgstr "ჩაშენებულ დამთვალიერებელში ჩვენება"

#: filegroupdetails.cpp:27 filetypedetails.cpp:134
#, kde-format
msgid "Show file in separate viewer"
msgstr "ცალკე დამთვალიერებელში ჩვენება"

#: filegroupdetails.cpp:36
#, kde-format
msgid ""
"Here you can configure what the Konqueror file manager will do when you "
"click on a file belonging to this group. Konqueror can display the file in "
"an embedded viewer or start up a separate application. You can change this "
"setting for a specific file type in the 'Embedding' tab of the file type "
"configuration. Dolphin  shows files always in a separate viewer"
msgstr ""

#: filetypedetails.cpp:61
#, kde-format
msgid ""
"This button displays the icon associated with the selected file type. Click "
"on it to choose a different icon."
msgstr ""

#: filetypedetails.cpp:67
#, kde-format
msgid "Filename Patterns"
msgstr "ფაილის სახელის შაბლონები"

#: filetypedetails.cpp:79
#, kde-format
msgid ""
"This box contains a list of patterns that can be used to identify files of "
"the selected type. For example, the pattern *.txt is associated with the "
"file type 'text/plain'; all files ending in '.txt' are recognized as plain "
"text files."
msgstr ""

#: filetypedetails.cpp:87 filetypesview.cpp:103 kservicelistwidget.cpp:110
#, kde-format
msgid "Add..."
msgstr "დამატება..."

#: filetypedetails.cpp:92
#, kde-format
msgid "Add a new pattern for the selected file type."
msgstr "მონიშნული ფაილის ტიპისთვის ახალი შაბლონის დამატება."

#: filetypedetails.cpp:94 kservicelistwidget.cpp:124
#, kde-format
msgid "Remove"
msgstr "წაშლა"

#: filetypedetails.cpp:99
#, kde-format
msgid "Remove the selected filename pattern."
msgstr "მონიშნული ფაილის სახელის შაბლონს წაშლა."

#: filetypedetails.cpp:110
#, kde-format
msgid "Description:"
msgstr "აღწერა:"

#: filetypedetails.cpp:115
#, kde-format
msgid ""
"You can enter a short description for files of the selected file type (e.g. "
"'HTML Page'). This description will be used by applications like Konqueror "
"to display directory content."
msgstr ""

#: filetypedetails.cpp:128
#, kde-format
msgid "Left Click Action in Konqueror"
msgstr "მარცხენა ღილაკის წკაპის ქმედება Konqtueror-ში"

#: filetypedetails.cpp:137
#, kde-format
msgid "Ask whether to save to disk instead (only for Konqueror browser)"
msgstr ""

#: filetypedetails.cpp:153
#, kde-format
msgid ""
"Here you can configure what the Konqueror file manager will do when you "
"click on a file of this type. Konqueror can either display the file in an "
"embedded viewer, or start up a separate application. If set to 'Use settings "
"for G group', the file manager will behave according to the settings of the "
"group G to which this type belongs; for instance, 'image' if the current "
"file type is image/png. Dolphin always shows files in a separate viewer."
msgstr ""

#: filetypedetails.cpp:165
#, kde-format
msgid "&General"
msgstr "&ზოგადი"

#: filetypedetails.cpp:166
#, kde-format
msgid "&Embedding"
msgstr "&ჩაშენება"

#: filetypedetails.cpp:207
#, kde-format
msgid "Add New Extension"
msgstr "ახალი გაფართოების დამატება"

#: filetypedetails.cpp:207
#, kde-format
msgid "Extension:"
msgstr "გაფართოება:"

#: filetypedetails.cpp:325
#, kde-format
msgid "File type %1"
msgstr "ფაილის ტიპი: %1"

#: filetypedetails.cpp:333
#, kde-format
msgid "Use settings for '%1' group"
msgstr "პარამეტრების გამოყენება '%1' ჯგუფისთვის"

#: filetypesview.cpp:44
#, kde-format
msgid ""
"<p><h1>File Associations</h1> This module allows you to choose which "
"applications are associated with a given type of file. File types are also "
"referred to as MIME types (MIME is an acronym which stands for "
"\"Multipurpose Internet Mail Extensions\").</p><p> A file association "
"consists of the following: <ul><li>Rules for determining the MIME-type of a "
"file, for example the filename pattern *.png, which means 'all files with "
"names that end in .png', is associated with the MIME type \"image/png\";</"
"li> <li>A short description of the MIME-type, for example the description of "
"the MIME type \"image/png\" is simply 'PNG image';</li> <li>An icon to be "
"used for displaying files of the given MIME-type, so that you can easily "
"identify the type of file in a file manager or file-selection dialog (at "
"least for the types you use often);</li> <li>A list of the applications "
"which can be used to open files of the given MIME-type -- if more than one "
"application can be used then the list is ordered by priority.</li></ul> You "
"may be surprised to find that some MIME types have no associated filename "
"patterns; in these cases, KDE is able to determine the MIME-type by directly "
"examining the contents of the file.</p>"
msgstr ""

#: filetypesview.cpp:74
#, kde-format
msgid "Search for file type or filename pattern..."
msgstr "ფაილის ტიპის ან ნიმუშის მოძებნა..."

#: filetypesview.cpp:80
#, kde-format
msgid ""
"Enter a part of a filename pattern, and only file types with a matching file "
"pattern will appear in the list. Alternatively, enter a part of a file type "
"name as it appears in the list."
msgstr ""

#: filetypesview.cpp:88
#, kde-format
msgid "Known Types"
msgstr "ცნობილი ტიპები"

#: filetypesview.cpp:94
#, kde-format
msgid ""
"Here you can see a hierarchical list of the file types which are known on "
"your system. Click on the '+' sign to expand a category, or the '-' sign to "
"collapse it. Select a file type (e.g. text/html for HTML files) to view/edit "
"the information for that file type using the controls on the right."
msgstr ""

#: filetypesview.cpp:108
#, kde-format
msgid "Click here to add a new file type."
msgstr "ფაილის ახალი ტიპის დასამატებლად აქ დააწკაპუნეთ."

#: filetypesview.cpp:110 filetypesview.cpp:376
#, kde-format
msgid "&Remove"
msgstr "&წაშლა"

#: filetypesview.cpp:134
#, kde-format
msgid "Select a file type by name or by extension"
msgstr "აირჩიეთ ფაილის ტიპი მისი სახელით ან გაფართოებით"

#: filetypesview.cpp:369
#, kde-format
msgid "&Revert"
msgstr "&დაბრუნება"

#: filetypesview.cpp:370
#, kde-format
msgid "Revert this file type to its initial system-wide definition"
msgstr "ამ ფაილის ტიპის მის სისტემაში საწყის აღწერაზე დაბრუნება"

#: filetypesview.cpp:372
#, kde-format
msgid ""
"Click here to revert this file type to its initial system-wide definition, "
"which undoes any changes made to the file type. Note that system-wide file "
"types cannot be deleted. You can however empty their pattern list, to "
"minimize the chances of them being used (but the file type determination "
"from file contents can still end up using them)."
msgstr ""

#: filetypesview.cpp:377
#, kde-format
msgid "Delete this file type definition completely"
msgstr "ფაილის ამ ტიპის სრულად წაშლა"

#: filetypesview.cpp:379
#, kde-format
msgid ""
"Click here to delete this file type definition completely. This is only "
"possible for user-defined file types. System-wide file types cannot be "
"deleted. You can however empty their pattern list, to minimize the chances "
"of them being used (but the file type determination from file contents can "
"still end up using them)."
msgstr ""

#: keditfiletype.cpp:120
#, kde-format
msgid "File Type Editor"
msgstr "ფაილის ტიპის რედაქტორი"

#: keditfiletype.cpp:122
#, kde-format
msgid ""
"KDE file type editor - simplified version for editing a single file type"
msgstr ""
"KDE-ის ფაილის ტიპების რედაქტორი - გამარტივებული ვერსია ერთი ფაილის ტიპის "
"ჩასასწორებლად"

#: keditfiletype.cpp:124
#, kde-format
msgid "(c) 2000, KDE developers"
msgstr "(c) 2000, The KDE -ის პროგრამისტები"

#: keditfiletype.cpp:125
#, kde-format
msgid "Preston Brown"
msgstr "Preston Brown"

#: keditfiletype.cpp:126
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: keditfiletype.cpp:132
#, kde-format
msgid "Makes the dialog transient for the window specified by winid"
msgstr ""

#: keditfiletype.cpp:134
#, kde-format
msgid "File type to edit (e.g. text/html)"
msgstr "ჩასასწორებელი ფაილის ტიპი (მაგ: text/html)"

#: keditfiletype.cpp:165
#, kde-format
msgid "%1 File"
msgstr "%1 ფაილი"

#: keditfiletype.cpp:192
#, kde-format
msgid "Edit File Type %1"
msgstr "ფაილის ტიპის ჩასწორება: %1"

#: keditfiletype.cpp:194
#, kde-format
msgid "Create New File Type %1"
msgstr "ფაილის ახალი ტიპის შექმნა: %1"

#: kservicelistwidget.cpp:36 kservicelistwidget.cpp:52
#, kde-format
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: kservicelistwidget.cpp:57
#, kde-format
msgid "Application Preference Order"
msgstr "აპლიკაციების მისაღები მიმდევრობა"

#: kservicelistwidget.cpp:57
#, kde-format
msgid "Services Preference Order"
msgstr "სერვისების მისაღები მიმდევრობა"

#: kservicelistwidget.cpp:68
#, kde-format
msgid ""
"This is a list of applications associated with files of the selected file "
"type. This list is shown in Konqueror's context menus when you select \"Open "
"With...\". If more than one application is associated with this file type, "
"then the list is ordered by priority with the uppermost item taking "
"precedence over the others."
msgstr ""

#: kservicelistwidget.cpp:73
#, kde-format
msgid ""
"This is a list of services associated with files of the selected file type. "
"This list is shown in Konqueror's context menus when you select a \"Preview "
"with...\" option. If more than one service is associated with this file "
"type, then the list is ordered by priority with the uppermost item taking "
"precedence over the others."
msgstr ""

#: kservicelistwidget.cpp:85
#, kde-format
msgid "Move &Up"
msgstr "აწევა"

#: kservicelistwidget.cpp:91
#, kde-format
msgid ""
"Assigns a higher priority to the selected\n"
"application, moving it up in the list. Note:  This\n"
"only affects the selected application if the file type is\n"
"associated with more than one application."
msgstr ""

#: kservicelistwidget.cpp:95
#, kde-format
msgid ""
"Assigns a higher priority to the selected\n"
"service, moving it up in the list."
msgstr ""

#: kservicelistwidget.cpp:98
#, kde-format
msgid "Move &Down"
msgstr "ჩამოწევა"

#: kservicelistwidget.cpp:103
#, kde-format
msgid ""
"Assigns a lower priority to the selected\n"
"application, moving it down in the list. Note: This \n"
"only affects the selected application if the file type is\n"
"associated with more than one application."
msgstr ""

#: kservicelistwidget.cpp:107
#, kde-format
msgid ""
"Assigns a lower priority to the selected\n"
"service, moving it down in the list."
msgstr ""

#: kservicelistwidget.cpp:115
#, kde-format
msgid "Add a new application for this file type."
msgstr "ამ ფაილის ტიპისთვის ახალი აპლიკაციის დამატება."

#: kservicelistwidget.cpp:117
#, kde-format
msgid "Edit..."
msgstr "ჩასწორება..."

#: kservicelistwidget.cpp:122
#, kde-format
msgid "Edit command line of the selected application."
msgstr "მონიშნული აპლიკაციის ბრძანების სტრიქონის ჩასწორება."

#: kservicelistwidget.cpp:129
#, kde-format
msgid "Remove the selected application from the list."
msgstr "მონიშნული აპლიკაციის სიიდან წაშლა."

#: kservicelistwidget.cpp:152
#, kde-format
msgctxt "No applications associated with this file type"
msgid "None"
msgstr "არაფერი"

#: kservicelistwidget.cpp:154
#, kde-format
msgctxt "No components associated with this file type"
msgid "None"
msgstr "არაფერი"

#: kserviceselectdlg.cpp:42
#, kde-format
msgid "Add Service"
msgstr "სერვისის დამატება"

#: kserviceselectdlg.cpp:46
#, kde-format
msgid "Select service:"
msgstr "აირჩიეთ სერვისი:"

#: newtypedlg.cpp:25
#, kde-format
msgid "Create New File Type"
msgstr "ფაილის ახალი ტიპის შექმნა"

#: newtypedlg.cpp:30
#, kde-format
msgid "Group:"
msgstr "ჯგუფი:"

#: newtypedlg.cpp:39
#, kde-format
msgid "Select the category under which the new file type should be added."
msgstr ""

#: newtypedlg.cpp:44
#, kde-format
msgid "Type name:"
msgstr "ტიპის სახელი:"

#: newtypedlg.cpp:50
#, kde-format
msgid ""
"Type the name of the file type. For instance, if you selected 'image' as "
"category and you type 'custom' here, the file type 'image/custom' will be "
"created."
msgstr ""
