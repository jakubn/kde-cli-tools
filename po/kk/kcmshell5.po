# translation of kcmshell.po to Kazakh
#
# Sairan Kikkarin <sairan@computer.org>, 2005, 2007, 2011.
# Sairan Kikkarin <sairan(at)computer.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmshell\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-29 00:46+0000\n"
"PO-Revision-Date: 2011-06-25 04:29+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Сайран Киккарин"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sairan@computer.org"

#: main.cpp:183
#, kde-format
msgid "System Settings Module"
msgstr ""

#: main.cpp:185
#, fuzzy, kde-format
#| msgid "A tool to start single KDE control modules"
msgid "A tool to start single system settings modules"
msgstr "Жалғыз KDE бақылау модульдерін жегу құралы"

#: main.cpp:187
#, fuzzy, kde-format
#| msgid "(c) 1999-2004, The KDE Developers"
msgid "(c) 1999-2016, The KDE Developers"
msgstr "(c) 1999-2004, KDE жасаушылары"

#: main.cpp:189
#, kde-format
msgid "Frans Englich"
msgstr "Frans Englich"

#: main.cpp:189
#, kde-format
msgid "Maintainer"
msgstr "Жетілдірушісі"

#: main.cpp:190
#, kde-format
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"

#: main.cpp:191
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hoelzer-Kluepfel"

#: main.cpp:192
#, kde-format
msgid "Matthias Elter"
msgstr "Matthias Elter"

#: main.cpp:193
#, kde-format
msgid "Matthias Ettrich"
msgstr "Matthias Ettrich"

#: main.cpp:194
#, kde-format
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:200
#, kde-format
msgid "List all possible modules"
msgstr "Барлық модульдер тізімі көрсетілсін"

#: main.cpp:201
#, kde-format
msgid "Configuration module to open"
msgstr "Ашатын баптау модулі"

#: main.cpp:202
#, kde-format
msgid "Specify a particular language"
msgstr "Керек тілді келтіру"

#: main.cpp:203
#, kde-format
msgid "Do not display main window"
msgstr "Негізгі терезесі көрсетілмесін"

#: main.cpp:204
#, kde-format
msgid "Arguments for the module"
msgstr "Модульдың аргументтері"

#: main.cpp:205
#, kde-format
msgid "Use a specific icon for the window"
msgstr ""

#: main.cpp:206
#, kde-format
msgid "Use a specific caption for the window"
msgstr ""

#: main.cpp:215
#, kde-format
msgid ""
"--lang is deprecated. Please set the LANGUAGE environment variable instead"
msgstr ""

#: main.cpp:219
#, kde-format
msgid "The following modules are available:"
msgstr "Қол жеткізер модульдері келесі:"

#: main.cpp:239
#, kde-format
msgid "No description available"
msgstr "Сипаттамасы жоқ"

#: main.cpp:303
#, fuzzy, kde-format
#| msgid ""
#| "Could not find module '%1'. See kcmshell4 --list for the full list of "
#| "modules."
msgid ""
"Could not find module '%1'. See kcmshell5 --list for the full list of "
"modules."
msgstr ""
"'%1' модулі табылмады. kcmshell4 --list деп модульдерінің толық тізімін "
"қараңыз."

#~ msgid "KDE Control Module"
#~ msgstr "KDE Басқару модулі"
