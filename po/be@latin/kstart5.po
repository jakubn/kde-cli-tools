# translation of kstart.po to Belarusian Latin
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the kstart package.
#
# Ihar Hrachyshka <ihar.hrachyshka@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kstart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-17 00:51+0000\n"
"PO-Revision-Date: 2008-09-08 22:22+0300\n"
"Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>\n"
"Language-Team: Belarusian Latin <i18n@mova.org>\n"
"Language: be@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || n%10>=5 && n%10<=9 || n"
"%100>=11 && n%100<=14 ? 2 : 3);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Ihar Hračyška"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ihar.hrachyshka@gmail.com"

#: kstart.cpp:255
#, kde-format
msgid "KStart"
msgstr "KStart"

#: kstart.cpp:257
#, kde-format
msgid ""
"Utility to launch applications with special window properties \n"
"such as iconified, maximized, a certain virtual desktop, a special "
"decoration\n"
"and so on."
msgstr ""
"Pryłada, jakaja dazvalaje ŭklučać aplikacyi z asablivymi ŭłaścivaściami "
"akna,\n"
"prykładam, źminimalizavanymi, zmaksymalizavanymi, na peŭnym virtualnym "
"stale,\n"
"z peŭnaj azdobaj akna itp."

#: kstart.cpp:262
#, kde-format
msgid "(C) 1997-2000 Matthias Ettrich (ettrich@kde.org)"
msgstr "© 1997-2000 Matthias Ettrich (ettrich@kde.org)"

#: kstart.cpp:264
#, kde-format
msgid "Matthias Ettrich"
msgstr "Matthias Ettrich"

#: kstart.cpp:265
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kstart.cpp:266
#, kde-format
msgid "Richard J. Moore"
msgstr "Richard J. Moore"

#: kstart.cpp:271
#, kde-format
msgid "Command to execute"
msgstr "Zahad dla vykanańnia"

#: kstart.cpp:275
#, kde-format
msgid ""
"Alternative to <command>: desktop file path to start. D-Bus service will be "
"printed to stdout. Deprecated: use --application"
msgstr ""

#: kstart.cpp:278
#, kde-format
msgid "Alternative to <command>: desktop file to start."
msgstr ""

#: kstart.cpp:281
#, kde-format
msgid "Optional URL to pass <desktopfile>, when using --service"
msgstr ""

#: kstart.cpp:284
#, kde-format
msgid "A regular expression matching the window title"
msgstr "Rehularny vyraz, jakomu adpaviadaje zahałovak akna"

#: kstart.cpp:286
#, kde-format
msgid ""
"A string matching the window class (WM_CLASS property)\n"
"The window class can be found out by running\n"
"'xprop | grep WM_CLASS' and clicking on a window\n"
"(use either both parts separated by a space or only the right part).\n"
"NOTE: If you specify neither window title nor window class,\n"
"then the very first window to appear will be taken;\n"
"omitting both options is NOT recommended."
msgstr ""
"Tekst, jaki adpaviadaje klasie akna (ułaścivaść „WM_CLASS”)\n"
"Ty možaš daviedacca, jakaja ŭ akna klasa, vykanaŭšy prahramu\n"
"„xprop | grep WM_CLASS” i kliknuŭšy ŭ hetaje akno (vykarystaj albo\n"
"abiedźvie častki, padzielenyja prabiełam, albo tolki pravuju častku).\n"
"Uvaha: ź nieaznačanymi zahałoŭkam i klasaj akna\n"
"budzie ŭziataje pieršaje ŭźnikłaje akno.\n"
"My nia raim hetak rabić."

#: kstart.cpp:295
#, kde-format
msgid "Desktop on which to make the window appear"
msgstr "Rabočy stoł, na jakim pavinna ŭźniknuć akno"

#: kstart.cpp:297
#, kde-format
msgid ""
"Make the window appear on the desktop that was active\n"
"when starting the application"
msgstr ""
"Akno ŭźniknie na tym stale,\n"
"dzie była ŭklučanaja aplikacyja."

#: kstart.cpp:298
#, kde-format
msgid "Make the window appear on all desktops"
msgstr "Akno ŭźniknie na ŭsich rabočych stałach."

#: kstart.cpp:299
#, kde-format
msgid "Iconify the window"
msgstr "Zminimalizuj akno"

#: kstart.cpp:300
#, kde-format
msgid "Maximize the window"
msgstr "Zmaksymalizuj akno"

#: kstart.cpp:301
#, kde-format
msgid "Maximize the window vertically"
msgstr "Zmaksymalizuj akno vertykalna"

#: kstart.cpp:302
#, kde-format
msgid "Maximize the window horizontally"
msgstr "Zmaksymalizuj akno haryzantalna"

#: kstart.cpp:303
#, kde-format
msgid "Show window fullscreen"
msgstr "Pakažy akno na ŭvieś ekran"

#: kstart.cpp:305
#, kde-format
msgid ""
"The window type: Normal, Desktop, Dock, Toolbar, \n"
"Menu, Dialog, TopMenu or Override"
msgstr ""
"Typ akna: „Normal” (zvyčajnaje), „Desktop” (stoł), „Dock” (unutranaje), "
"„Toolbar” (panel pryładździa), \n"
"„Menu” (menu), „Dialog” (dyjalohavaje), „TopMenu” (menu vyššaha roŭniu) ci "
"„Override” (nadpisańnie)."

#: kstart.cpp:308
#, kde-format
msgid ""
"Jump to the window even if it is started on a \n"
"different virtual desktop"
msgstr ""
"Pieraklučy ŭ akno, navat kali jano ŭźnikła ŭ inšym\n"
"virtualnym rabočym stale"

#: kstart.cpp:311
#, kde-format
msgid "Try to keep the window above other windows"
msgstr "Trymaj nad inšymi voknami"

#: kstart.cpp:313
#, kde-format
msgid "Try to keep the window below other windows"
msgstr "Trymaj pad inšymi voknami"

#: kstart.cpp:314
#, kde-format
msgid "The window does not get an entry in the taskbar"
msgstr "Akno biez pałasy ŭ paneli zadańniaŭ"

#: kstart.cpp:315
#, kde-format
msgid "The window does not get an entry on the pager"
msgstr "Akno biaz ramki ŭ paneli stałoŭ"

#: kstart.cpp:329
#, kde-format
msgid "No command specified"
msgstr "Zahad nie padadzieny"

#~ msgid "The window is sent to the system tray in Kicker"
#~ msgstr "Akno zminimalizavanaje ŭ systemny trej u paneli"
