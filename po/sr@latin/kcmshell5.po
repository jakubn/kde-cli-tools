# Translation of kcmshell5.po into Serbian.
# Toplica Tanaskovic <toptan@kde.org.yu>, 2005.
# Chusslove Illich <caslav.ilic@gmx.net>, 2007, 2010, 2011, 2014, 2015, 2016, 2017.
# Dalibor Djuric <dalibor.djuric@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmshell5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-10-22 03:11+0200\n"
"PO-Revision-Date: 2017-09-28 17:58+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Toplica Tanasković,Časlav Ilić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "toptan@kde.org.yu,caslav.ilic@gmx.net"

#: main.cpp:193
#, kde-format
msgid "System Settings Module"
msgstr "Modul Sistemskih postavki"

#: main.cpp:195
#, kde-format
msgid "A tool to start single system settings modules"
msgstr "Samostalno pokretanje modula Sistemskih postavki"

#: main.cpp:197
#, kde-format
msgid "(c) 1999-2016, The KDE Developers"
msgstr "© 1999–2016, programeri KDE‑a"

#: main.cpp:199
#, kde-format
msgid "Frans Englich"
msgstr "Frans Englih"

#: main.cpp:199
#, kde-format
msgid "Maintainer"
msgstr "održavalac"

#: main.cpp:200
#, kde-format
msgid "Daniel Molkentin"
msgstr "Danijel Molkentin"

#: main.cpp:201
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matijas Helcer-Klipfel"

#: main.cpp:202
#, kde-format
msgid "Matthias Elter"
msgstr "Matijas Elter"

#: main.cpp:203
#, kde-format
msgid "Matthias Ettrich"
msgstr "Matijas Etrih"

#: main.cpp:204
#, kde-format
msgid "Waldo Bastian"
msgstr "Valdo Bastijan"

#: main.cpp:210
#, kde-format
msgid "List all possible modules"
msgstr "Spisak svih mogućih modula"

#: main.cpp:211
#, kde-format
msgid "Configuration module to open"
msgstr "Kontrolni modul koji treba otvoriti"

#: main.cpp:212
#, kde-format
msgid "Specify a particular language"
msgstr "Pokreni na određenom jeziku"

#: main.cpp:213
#, kde-format
msgid "Do not display main window"
msgstr "Bez glavnog prozora"

#: main.cpp:214
#, kde-format
msgid "Arguments for the module"
msgstr "Argumenti za modul"

#: main.cpp:215
#, kde-format
msgid "Use a specific icon for the window"
msgstr "Koristi zadatu ikonicu za prozor"

#: main.cpp:216
#, kde-format
msgid "Use a specific caption for the window"
msgstr "Koristi zadati natpis za prozor"

#: main.cpp:225
#, kde-format
msgid ""
"--lang is deprecated. Please set the LANGUAGE environment variable instead"
msgstr "--lang je prevaziđena. Koristite promenljivu okruženja LANGUAGE."

#: main.cpp:229
#, kde-format
msgid "The following modules are available:"
msgstr "Dostupni su sledeći moduli:"

#: main.cpp:246
#, kde-format
msgid "No description available"
msgstr "Nema opisa"

#: main.cpp:277
#, kde-format
msgid ""
"Could not find module '%1'. See kcmshell5 --list for the full list of "
"modules."
msgstr ""
"Ne mogu da nađem modul „%1“. Izvršite „kcmshell5 --list“ za spisak svih "
"modula."
