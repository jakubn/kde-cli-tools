# Vietnamese translation for kstart.
# Copyright © 2007 KDE i18n Project for Vietnamese.
#
# Nguyễn Hưng Vũ <Vu.Hung@techviet.com>, 2002.
# Phan Vĩnh Thịnh <teppi82@gmail.com>, 2006.
# Lê Hoàng Phương <herophuong93@gmail.com>, 2013.
# Phu Hung Nguyen <phu.nguyen@kdemail.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kstart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-17 00:51+0000\n"
"PO-Revision-Date: 2022-05-05 14:20+0200\n"
"Last-Translator: Phu Hung Nguyen <phu.nguyen@kdemail.net>\n"
"Language-Team: Vietnamese <kde-l10n-vi@kde.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Nguyễn Hùng Phú,Lê Hoàng Phương"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "phu.nguyen@kdemail.net,herophuong93@gmail.com"

#: kstart.cpp:255
#, kde-format
msgid "KStart"
msgstr "KStart"

#: kstart.cpp:257
#, kde-format
msgid ""
"Utility to launch applications with special window properties \n"
"such as iconified, maximized, a certain virtual desktop, a special "
"decoration\n"
"and so on."
msgstr ""
"Tiện ích để khởi chạy các ứng dụng với các thuộc tính cửa sổ đặc biệt \n"
"như biểu tượng hoá, phóng to, bàn làm việc ảo xác định, trang trí đặc biệt\n"
"và các thứ khác."

#: kstart.cpp:262
#, kde-format
msgid "(C) 1997-2000 Matthias Ettrich (ettrich@kde.org)"
msgstr "(C) 1997-2000 Matthias Ettrich (ettrich@kde.org)"

#: kstart.cpp:264
#, kde-format
msgid "Matthias Ettrich"
msgstr "Matthias Ettrich"

#: kstart.cpp:265
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kstart.cpp:266
#, kde-format
msgid "Richard J. Moore"
msgstr "Richard J. Moore"

#: kstart.cpp:271
#, kde-format
msgid "Command to execute"
msgstr "Lệnh để thực thi"

#: kstart.cpp:275
#, kde-format
msgid ""
"Alternative to <command>: desktop file path to start. D-Bus service will be "
"printed to stdout. Deprecated: use --application"
msgstr ""
"Thay thế cho <command>: đường dẫn tệp desktop để khởi động. Dịch vụ D-Bus sẽ "
"được in ra đầu ra tiêu chuẩn. Phế: hãy dùng --application"

#: kstart.cpp:278
#, kde-format
msgid "Alternative to <command>: desktop file to start."
msgstr "Thay thế cho <command>: tệp desktop để khởi động."

#: kstart.cpp:281
#, kde-format
msgid "Optional URL to pass <desktopfile>, when using --service"
msgstr "URL tuỳ chọn để truyền <desktopfile>, khi dùng --service"

#: kstart.cpp:284
#, kde-format
msgid "A regular expression matching the window title"
msgstr "Một biểu thức chính quy khớp với tiêu đề cửa sổ"

#: kstart.cpp:286
#, kde-format
msgid ""
"A string matching the window class (WM_CLASS property)\n"
"The window class can be found out by running\n"
"'xprop | grep WM_CLASS' and clicking on a window\n"
"(use either both parts separated by a space or only the right part).\n"
"NOTE: If you specify neither window title nor window class,\n"
"then the very first window to appear will be taken;\n"
"omitting both options is NOT recommended."
msgstr ""
"Một chuỗi khớp với lớp cửa sổ (thuộc tính WM_CLASS)\n"
"Có thể tìm ra lớp cửa sổ bằng cách chạy\n"
"'xprop | grep WM_CLASS' rồi bấm vào một cửa sổ\n"
"(dùng cả hai phần, cách nhau bởi dấu cách, hoặc chỉ phần bên phải).\n"
"LƯU Ý: Nếu bạn không chỉ định cả tiêu đề cửa sổ lẫn lớp cửa sổ, thì\n"
"cửa sổ đầu tiên xuất hiện sẽ được lấy;\n"
"KHÔNG nên bỏ qua cả hai lựa chọn đó."

#: kstart.cpp:295
#, kde-format
msgid "Desktop on which to make the window appear"
msgstr "Bàn làm việc để làm cửa sổ xuất hiện"

#: kstart.cpp:297
#, kde-format
msgid ""
"Make the window appear on the desktop that was active\n"
"when starting the application"
msgstr ""
"Làm cửa sổ xuất hiện trên bàn làm việc mà đang hoạt động\n"
"khi chạy ứng dụng"

#: kstart.cpp:298
#, kde-format
msgid "Make the window appear on all desktops"
msgstr "Làm cửa sổ xuất hiện trên tất cả các bàn làm việc"

#: kstart.cpp:299
#, kde-format
msgid "Iconify the window"
msgstr "Biểu tượng hoá cửa sổ"

#: kstart.cpp:300
#, kde-format
msgid "Maximize the window"
msgstr "Phóng to cửa sổ"

#: kstart.cpp:301
#, kde-format
msgid "Maximize the window vertically"
msgstr "Phóng to cửa sổ theo chiều dọc"

#: kstart.cpp:302
#, kde-format
msgid "Maximize the window horizontally"
msgstr "Phóng to cửa sổ theo chiều ngang"

#: kstart.cpp:303
#, kde-format
msgid "Show window fullscreen"
msgstr "Hiện cửa sổ toàn màn hình"

#: kstart.cpp:305
#, kde-format
msgid ""
"The window type: Normal, Desktop, Dock, Toolbar, \n"
"Menu, Dialog, TopMenu or Override"
msgstr ""
"Kiểu cửa sổ: Thường, Bàn làm việc, Giá, Thanh công cụ, \n"
"Trình đơn, Hộp thoại, Trình đơn đầu hoặc Vượt hiệu lực"

#: kstart.cpp:308
#, kde-format
msgid ""
"Jump to the window even if it is started on a \n"
"different virtual desktop"
msgstr ""
"Nhảy đến cửa sổ kể cả nếu nó được khởi động\n"
"ở một bàn làm việc ảo khác "

#: kstart.cpp:311
#, kde-format
msgid "Try to keep the window above other windows"
msgstr "Giữ cửa sổ nằm trên các cửa sổ khác"

#: kstart.cpp:313
#, kde-format
msgid "Try to keep the window below other windows"
msgstr "Giữ cửa sổ nằm dưới các cửa sổ khác"

#: kstart.cpp:314
#, kde-format
msgid "The window does not get an entry in the taskbar"
msgstr "Cửa sổ sẽ không có một mục trên thanh tác vụ"

#: kstart.cpp:315
#, kde-format
msgid "The window does not get an entry on the pager"
msgstr "Cửa sổ sẽ không có một mục trong trình tổng quan"

#: kstart.cpp:329
#, kde-format
msgid "No command specified"
msgstr "Không có lệnh nào được chỉ định"
