# Translation of kcmshell.po to Irish
# Copyright (C) 1999,2003,2004, 2005 Free Software Foundation, Inc.
# Séamus Ó Ciardhuáin <seoc at iolfree dot ie>, 2003,2004,2005
# Kevin Scannell <kscanne@gmail.com>, 2004, 2005, 2009
msgid ""
msgstr ""
"Project-Id-Version: kcmshell\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-29 00:46+0000\n"
"PO-Revision-Date: 2005-01-16 00:43+0000\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Séamus Ó Ciardhuáin,Kevin Scannell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "seoc@iolfree.ie,kscanne@gmail.com"

#: main.cpp:183
#, kde-format
msgid "System Settings Module"
msgstr ""

#: main.cpp:185
#, fuzzy, kde-format
#| msgid "A tool to start single KDE control modules"
msgid "A tool to start single system settings modules"
msgstr "Uirlis chun modúl rialaithe singil de chuid KDE a thosú"

#: main.cpp:187
#, fuzzy, kde-format
#| msgid "(c) 1999-2004, The KDE Developers"
msgid "(c) 1999-2016, The KDE Developers"
msgstr "© 1999-2004, Forbróirí KDE"

#: main.cpp:189
#, kde-format
msgid "Frans Englich"
msgstr "Frans Englich"

#: main.cpp:189
#, kde-format
msgid "Maintainer"
msgstr "Cothaitheoir"

#: main.cpp:190
#, kde-format
msgid "Daniel Molkentin"
msgstr "Daniel Molkentin"

#: main.cpp:191
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "Matthias Hoelzer-Kluepfel"

#: main.cpp:192
#, kde-format
msgid "Matthias Elter"
msgstr "Matthias Elter"

#: main.cpp:193
#, kde-format
msgid "Matthias Ettrich"
msgstr "Matthias Ettrich"

#: main.cpp:194
#, kde-format
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:200
#, kde-format
msgid "List all possible modules"
msgstr "Liostáil na modúil go léir"

#: main.cpp:201
#, kde-format
msgid "Configuration module to open"
msgstr "Modúl cumraíochta le hoscailt"

#: main.cpp:202
#, kde-format
msgid "Specify a particular language"
msgstr "Sonraigh teanga áirithe"

#: main.cpp:203
#, kde-format
msgid "Do not display main window"
msgstr "Ná taispeáin an phríomhfhuinneog"

#: main.cpp:204
#, kde-format
msgid "Arguments for the module"
msgstr "Argóintí don mhodúl"

#: main.cpp:205
#, kde-format
msgid "Use a specific icon for the window"
msgstr ""

#: main.cpp:206
#, kde-format
msgid "Use a specific caption for the window"
msgstr ""

#: main.cpp:215
#, kde-format
msgid ""
"--lang is deprecated. Please set the LANGUAGE environment variable instead"
msgstr ""

#: main.cpp:219
#, kde-format
msgid "The following modules are available:"
msgstr "Tá na modúil seo a leanas ar fáil:"

#: main.cpp:239
#, kde-format
msgid "No description available"
msgstr "Níl cur síos ar fáil"

#: main.cpp:303
#, fuzzy, kde-format
#| msgid ""
#| "Could not find module '%1'. See kcmshell4 --list for the full list of "
#| "modules."
msgid ""
"Could not find module '%1'. See kcmshell5 --list for the full list of "
"modules."
msgstr ""
"Níorbh fhéidir modúl '%1' a aimsiú. Féach ar kcmshell4 --list chun an liosta "
"iomlán a fháil."

#~ msgid "KDE Control Module"
#~ msgstr "Modúl Rialaithe KDE"

#~ msgid "Embeds the module with buttons in window with id <id>"
#~ msgstr "Leabaigh an modúl le cnaipí i bhfuinneog leis an aitheantas <id>"

#~ msgid "Embeds the module without buttons in window with id <id>"
#~ msgstr "Leabaigh an modúl gan chnaipí i bhfuinneog leis an aitheantas <id>"

#~ msgid "Configure - %1"
#~ msgstr "Cumraíocht - %1"
